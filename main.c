/*--Galindo Mendoza Andr�s Abraham--
            --1AV7--
        --#ESIME ZACATENCO--
            --PONG--
    ING.CONTROL Y AUTOMATIZACION
*/



#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>
#define h 65
#define v 21

void Titulo();
void borde (char cuadro[v][h]);
void raqjug (char cuadro[v][h], int inijug, int finjug);
void raqia (char cuadro[v][h], int iniia, int finia);
void gotoxy(int x, int y);
void color(int c, int b);
void impresion(char cuadro[v][h], int pely, int mody);
void logica (char cuadro[v][h], int bolah, int bolay, int inijug, int finjug, int iniia, int finia, int modx, int mody, int modia);
void pel (char cuadro[v][h], int bolah, int bolay);
void leerCamp(char cuadro[v][h]);
void mapa(char cuadro[v][h], int bolah, int bolay, int inijug, int finjug, int iniia, int finia);
void imput(char cuadro[v][h], int *bolah, int *bolay, int *inijug, int *finjug, int *iniia, int *finia, int *modx, int *mody, int *modia, int *gol);
void update(char cuadro[v][h], int bolah, int bolay, int inijug, int finjug, int iniia, int finia);




int main(){
    int i;
  do{
     Titulo();
     gotoxy(22,25);printf("Que quiere hacer?:");
     scanf("%d",&i); fflush(stdin);
     system("cls");


    switch(i){
        case 1:{
        int a;
        do{

 int bolah, bolay, inijug, finjug, iniia, finia;
 int modx, mody, modia;
 char cuadro [v][h];



 bolah = 37; bolay = 10;
 inijug = 6; finjug = 12;
 iniia = 5; finia = 15;

 modx = -1;
 mody = -1;
 modia = -1;
 mapa(cuadro, bolah, bolay, inijug, finjug, iniia, finia);
 logica (cuadro, bolah, bolay, inijug, finjug, iniia, finia, modx, mody, modia);
        system("cls");
        color(13,0);gotoxy(30,0);printf("El juego a terminado :(\n\n");
        color(14,0);printf("1.- repetir\n\n");
        color(14,0);printf("2.-Salir\n\n");
        color(14,0);printf("Que desea hacer:");
        scanf("%d",&a);fflush(stdin);
}while(a!=2);
}
    break;
        default:
            printf("opcion invalida :(");
            break;

}
    system("cls");
}while(i!=2);

 system ("pause");
 return 0;
}


void mapa(char cuadro[v][h], int bolah, int bolay, int inijug, int finjug, int iniia, int finia){

 borde (cuadro);
 raqjug (cuadro, inijug, finjug);
 raqia (cuadro, iniia, finia);
 pel (cuadro, bolah, bolay);
}

void borde (char cuadro[v][h]){

 for (int i = 0; i < v; i++){

  for (int j = 0; j < h; j++){

   if (i == 0 || i == v-1 ){

    cuadro [i][j] = '_';
   }
   else if (j == 0 || j == h-1 ){

    cuadro [i][j] = 169;
   }
   else{

    cuadro [i][j] = ' ';
   }
  }
 }
}

void raqjug (char cuadro[v][h], int inijug, int finjug){

 for (int i = inijug; i < finjug; i++){

  for (int j = 2; j <= 3; j++){

   cuadro [i][j] = 69;
  }
 }
}

void raqia (char cuadro[v][h], int iniia, int finia){

 for (int i = iniia; i < finia; i++){

  for (int j = h-4; j <= h-3; j++){

   cuadro [i][j] = 69;
  }
 }
}

void pel (char cuadro[v][h], int bolah, int bolay){

 cuadro [bolay][bolah] = 'O';
}

void leerCamp(char cuadro[v][h]){

 for (int i = 0; i < v; i++){

  for (int j = 0; j < h; j++){
   printf ("%c", cuadro[i][j]);
  }
  printf ("\n");
 }
}

void logica (char cuadro[v][h], int bolah, int bolay, int inijug, int finjug, int iniia, int finia, int modx, int mody, int modia){

 int gol;
 gol = 0;
 do{

  impresion(cuadro, bolay, mody);
  imput(cuadro, &bolah, &bolay, &inijug, &finjug, &iniia, &finia, &modx, &mody, &modia, &gol);
  update(cuadro, bolah, bolay, inijug, finjug, iniia, finia);
  Sleep (10);
 } while(gol == 0);
}

void impresion(char cuadro[v][h], int bolay, int mody){

gotoxy(0,0);
 leerCamp(cuadro);
}

void imput(char cuadro[v][h], int *bolah, int *bolay, int *inijug, int *finjug, int *iniia, int *finia, int *modx, int *mody, int *modia, int *gol){

 char key;

 if (*bolay == 1 || *bolay == v-2){

  *mody *= -1;
 }

 if (*bolah == 1 || *bolah == h-2){

  *gol = 1;
 }

 if (*bolah == 4){

  for (int i = *inijug; i <= *finjug; i++){

   if (i == (*bolay)){

    *modx *= -1;
   }
  }
 }

 if (*bolah == h-5){

  for (int i = *iniia; i <= *finia; i++){

   if (i == (*bolay)){

    *modx *= -1;
   }
  }
 }

 if (*iniia == 1 || *finia == v-1){

  *modia *= -1;
 }


 if (*gol == 0){
  *bolah += (*modx);
  *bolay += (*mody);


  *iniia += (*modia);
  *finia += (*modia);

  if (kbhit() == 1){

   key = getch();

   if (key == 'w'){

    if (*inijug != 1){

     *inijug -= 1;
     *finjug -= 1;
    }
   }

   if (key == 's'){

    if (*finjug != v-1){

     *inijug += 1;
     *finjug += 1;
    }
   }
  }
 }

}

void update(char cuadro[v][h], int bolah, int bolay, int inijug, int finjug, int iniia, int finia){

 borde (cuadro);
 raqjug (cuadro, inijug, finjug);
 raqia (cuadro, iniia, finia);
 pel (cuadro, bolah, bolay);
}

void gotoxy(int x, int y)  {
     COORD coord;
     coord.X=x;
     coord.Y=y;
     SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);

}
void Titulo(){

color(13,0);   printf("\n\n");
        printf("       \t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 201,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,187);
        printf("       \t\t\t%c                       %c\n",186,186);
color(14,0);   printf("       \t\t\t%c     PONG :)    %c\n",186,186);
        printf("       \t\t\t%c         ---By ANDY---        %c\n",186,186);
        printf("       \t\t\t%c          -1AV7-        %c\n",186,186);
 color(13,0);        printf("       \t\t\t%c                       %c\n",186,186);
        printf("       \t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n\n", 200,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,188);
  color(14,0);      printf("\t\t\t   ==INSTRUCCIONES==\n\n");
 color(14,0);        printf("        1--Para ganar debes de anotar gol -->* \n\n");
        printf("        2--Cuidado con dejarte anotar un gol\n\n");
        printf("        3--El que anote el primer gol gana el juego\n\n");
        printf("        4--Para moverte utiliza w y s en tu teclado:\n\n");
        printf("        5--Presiona 1 para comenzar a jugar *--\n\n");
        printf("        5--Presiona 2 si quieres salir del juego--\n\n");
color(15,0);
}
void color(int c, int b){
HANDLE hConsole;
hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
FlushConsoleInputBuffer(hConsole);
SetConsoleTextAttribute(hConsole, c+16*b);
}
